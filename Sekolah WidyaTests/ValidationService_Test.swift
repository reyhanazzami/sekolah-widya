//
//  ValidationService_Test.swift
//  Sekolah WidyaTests
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import XCTest
@testable import Sekolah_Widya

class ValidationService_Test: XCTestCase {

    var validation: ValidationServices!
    
    override func setUp() {
        super.setUp()
        validation = ValidationServices()
    }
    
    override func tearDown() {
        validation = nil
        super.tearDown()
    }
    
    func test_is_value_email() throws{
        XCTAssertNoThrow(try validation.validateEmail("reyhan@gmail.com"))
    }
    
    func test_is_valid_email_fromat() throws{
        XCTAssertNoThrow(try validation.validateEmail("reyhan@gmail.com"))
    }
    
    func test_is_null_email() throws{
        let expected = ValidationError.invalidValue
        var error: ValidationError?
        
        XCTAssertThrowsError(try validation.validateEmail(nil)){thrown in
            error = thrown as? ValidationError
        }
        XCTAssertEqual(expected, error)
        XCTAssertEqual(expected.errorDescription, error?.errorDescription)
    }
    
    func test_isNot_valid_email_format() throws{
        let expected = ValidationError.emailFormatWrong
        var error: ValidationError?
        
        XCTAssertThrowsError(try validation.validateEmail("@mail.com")){thrown in
            error = thrown as? ValidationError
        }
        XCTAssertEqual(expected, error)
        XCTAssertEqual(expected.errorDescription, error?.errorDescription)
    }
    
    func test_is_valid_password() throws{
        XCTAssertNoThrow(try validation.validatePassword("password"))
    }
    
    func test_isNot_valid_passwordTooShort() throws{
        let expected = ValidationError.passwordTooShort
        var error: ValidationError?
        
        XCTAssertThrowsError(try validation.validatePassword("pass")){thrown in
            error = thrown as? ValidationError
        }
        XCTAssertEqual(expected, error)
        XCTAssertEqual(expected.errorDescription, error?.errorDescription)
    }
    
    func test_is_valid_passwordConfirmation() throws{
        XCTAssertNoThrow(try validation.validatePasswordConfirmation("password123", "password123"))
    }
    
    func test_isNot_valid_passwordNotmatch() throws{
        let expected = ValidationError.passwordConfirmationNotMatch
        var error: ValidationError?
        
        XCTAssertThrowsError(try validation.validatePasswordConfirmation("password", "passasdfasdf")){thrown in
            error = thrown as? ValidationError
        }
        XCTAssertEqual(expected, error)
        XCTAssertEqual(expected.errorDescription, error?.errorDescription)
    }
    
    func test_valid_phoneNumber(){
        XCTAssertNoThrow(try validation.validatePhone("081234567"))
    }
    
    func test_isNot_valid_phoneTooShort() throws{
        let expected = ValidationError.phoneNumberTooShort
        var error: ValidationError?
        
        XCTAssertThrowsError(try validation.validatePhone("1234")){thrown in
            error = thrown as? ValidationError
        }
        XCTAssertEqual(expected, error)
        XCTAssertEqual(expected.errorDescription, error?.errorDescription)
    }
    
    func test_isNot_valid_phoneTooLong() throws{
        let expected = ValidationError.phoneNumberTooLong
        var error: ValidationError?
        
        XCTAssertThrowsError(try validation.validatePhone("1234341234134234334234")){thrown in
            error = thrown as? ValidationError
        }
        XCTAssertEqual(expected, error)
        XCTAssertEqual(expected.errorDescription, error?.errorDescription)
    }
    
    func test_check_email_already_registered(){
        XCTAssertTrue(validation.validateEmailRegistered("mike@mail.com"))
    }
    
    func test_check_email_not_registered(){
        XCTAssertFalse(validation.validateEmailRegistered("noEmail@mail.com"))
    }
    
    
    
    

}
