//
//  APIService_Test.swift
//  Sekolah WidyaTests
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import XCTest
@testable import Sekolah_Widya

class APIService_Test: XCTestCase {

    var apiService: APIService!
    
    override func setUp() {
        super.setUp()
        apiService = APIService.shared()
    }
    
    override func tearDown() {
        apiService = nil
        super.tearDown()
    }
    
    func test_get_all_user(){
        apiService.request(endPoint: "user", method: .GET, parameters: nil, expecting: [User].self) { result in
            switch result{case .success(let users):
                XCTAssertNotNil(users)
            case .failure(_):
                break
            }
        }
    }
    func test_get_all_jurusan(){
        apiService.request(endPoint: "jurusan", method: .GET, parameters: nil, expecting: [Jurusan].self) { result in
            switch result{case .success(let jurusans):
                XCTAssertNotNil(jurusans)
            case .failure(_):
                break
            }
        }
    }
    func test_get_all_siswa_in_jurusan(){
        let idJurusan = 2
        apiService.request(endPoint: "jurusan/\(idJurusan)", method: .GET, parameters: nil, expecting: [User].self) { result in
            switch result{case .success(let users):
                XCTAssertNotNil(users)
            case .failure(_):
                break
            }
        }
    }
    
    func test_register_siswa(){
        let data = ["nama": "newTestRegister",
                    "email": "asdfsd@gmail.com",
                    "password": "newPassword",
                    "foto": nil,
                    "alamat": nil,
                    "noTelp": "08128374",
                    "sekolahAsal": "SMA 2",
                    "namaOrtu": "Fulan"
                    ]
        
        apiService.send(endPoint: "user/register", method: .POST, parameters: data) { result in
         
            switch result{
            case .success(let users):
                
                XCTAssertEqual(users.self, data.self)
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func test_update_user(){
        let data = User(id: nil, nama: "UPDATENAME", email: "asdfsd@gmail.com", foto: nil, alamat: "jakarta", noTelp: "088812638", sekolahAsal: "SMA 15", namaOrtu: "Fulan", createdAt: nil, updatedAt: nil)
        
        apiService.send(endPoint: "user/update", method: .POST, parameters: data) { result in
         
            switch result{
            case .success(let users):
                
                XCTAssertEqual(users.self, data.self)
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func test_Delete_user(){
        let data = ["id": "4"]
        
        apiService.send(endPoint: "user/delete", method: .POST, parameters: data) { result in
         
            switch result{
            case .success(let users):
                
                XCTAssertEqual(users.self, data.self)
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func test_send_email(){
        let data = EmailData(name: "Reyhan", email: "reyhanazzami@gmail.com", message: "Ini dari Swift")
        
        apiService.send(endPoint: "send_email", method: .POST, parameters: data) { result in
         
            switch result{
            case .success( let emailData):
                XCTAssertEqual(emailData.self, data.self)

                break
            case .failure(let error):
                print(error)
            }
        }
    }

}
