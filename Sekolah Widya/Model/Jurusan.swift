//
//  Jurusan.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 06/08/21.
//

import Foundation
struct Jurusan: Codable {
    let idPenjurusan: Int
    let namaPenjurusan: String
    let kuotaMaks: Int
    let updatedAt, createdAt: String

    enum CodingKeys: String, CodingKey {
        case idPenjurusan = "id_penjurusan"
        case namaPenjurusan = "nama_penjurusan"
        case kuotaMaks = "kuota_maks"
        case updatedAt = "updated_at"
        case createdAt = "created_at"
    }
}
