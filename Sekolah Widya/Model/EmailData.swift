//
//  EmailData.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 06/08/21.
//

import Foundation
struct EmailData: Codable, Equatable {
    let name: String
    let email: String
    let message: String
}
