//
//  User.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 06/08/21.
//

import Foundation

struct User: Codable, Equatable {
    let id: Int?
    let nama, email: String?
    let foto: String?
    let alamat, noTelp, sekolahAsal, namaOrtu: String?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, nama, email, foto, alamat, noTelp, sekolahAsal, namaOrtu
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
