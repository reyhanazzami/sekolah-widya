//
//  RegisterViewController.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import UIKit

class RegisterViewController: UIViewController {
    

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var textFieldNama: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldPasswordConfirmation: UITextField!
    @IBOutlet weak var buttonRegister: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func buttonRegisterTapped(_ sender: UIButton) {
    }

}
