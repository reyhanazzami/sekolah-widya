//
//  ProfileViewController.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import UIKit

class ProfileViewController: UIViewController {
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var imageSiswa: UIImageView!
    @IBOutlet weak var textFieldNama: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldAlamat: UITextField!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var textFieldSekolah: UITextField!
    @IBOutlet weak var textFieldNamaOrtu: UITextField!
    @IBOutlet weak var buttonRegis: UIButton!
    var siswaViewModel: SiswaViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    @IBAction func buttinRegisterTapped(_ sender: Any) {
        print(textFieldNama.text)
    }
    
    func initUI(){
        self.textFieldNama.text = siswaViewModel?.nama
        self.textFieldEmail.text = siswaViewModel?.email
        self.textFieldPhone.text = siswaViewModel?.telepon
        self.textFieldAlamat.text = siswaViewModel?.alamat
        self.textFieldSekolah.text = siswaViewModel?.sekolah
        self.textFieldNamaOrtu.text = siswaViewModel?.namaOrtu
    }

}
