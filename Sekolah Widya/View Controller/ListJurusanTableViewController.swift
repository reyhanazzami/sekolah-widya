//
//  ListJurusanTableViewController.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import UIKit

class ListJurusanTableViewController: UITableViewController {

    var model = [JurusanViewModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchJurusan()
    }
    
    func fetchJurusan(){
        let apiService = APIService.shared()

        apiService.request(endPoint: "jurusan",method: .GET, parameters: nil, expecting: [Jurusan].self) { result in
            switch result{
            case .success(let result):
                self.model = result.map({JurusanViewModel(jurusan: $0)})
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return model.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        let jurusan = model[indexPath.row]
        cell.textLabel?.text = jurusan.namaJurusan
        cell.detailTextLabel?.text = "Kuota Maksimal: \(jurusan.kuotaMaks ?? 0)"

        return cell
    }

}
