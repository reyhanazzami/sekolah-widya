//
//  RegisterViewController.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import UIKit

class RegisterProfileViewController: UIViewController {
    
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var textFieldAlamat: UITextField!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var textFieldSekolah: UITextField!
    @IBOutlet weak var textFieldNamaOrtu: UITextField!
    @IBOutlet weak var buttonRegister: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func buttonRegisterTapped(_ sender: UIButton) {
    }
    
}
