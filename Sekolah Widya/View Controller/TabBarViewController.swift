//
//  TabBarViewController.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import UIKit

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    var roleUser: roleUser = .admin
    var user: User?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        
        
        let tabOne = storyboard.instantiateViewController(withIdentifier: "ListJurusanTableViewController")
        let tabOneBarItem = UITabBarItem(title: "Jurusan", image: UIImage(systemName: "book"), selectedImage: UIImage(systemName: "book.fill"))
        tabOne.title = "Tools"
        let navigation1 = UINavigationController()
        navigation1.addChild(tabOne)
        navigation1.tabBarItem = tabOneBarItem
        
        
        let tabTwo = storyboard.instantiateViewController(withIdentifier: "ListSiswaTableViewController")
        tabTwo.title = "Siswa"
        let tabTwoBarItem = UITabBarItem(title: "Siswa", image: UIImage(systemName: "person.3"), selectedImage: UIImage(systemName: "person.3.fill"))
        let navigation = UINavigationController()
        navigation.addChild(tabTwo)
        navigation.tabBarItem = tabTwoBarItem
        
        
        let tabThree =  storyboard.instantiateViewController(identifier: "ProfileViewController") as! ProfileViewController
        tabThree.title = "Friend List"
        let viewModel = SiswaViewModel(siswa: self.user!)
        tabThree.siswaViewModel = viewModel
        let tabThreeBarItem = UITabBarItem(title: "Profile", image: UIImage(systemName: "person"), selectedImage: UIImage(systemName: "person.fill"))
        tabThree.tabBarItem = tabThreeBarItem
        
        switch roleUser{
        
        case .admin:
            self.viewControllers = [navigation1,navigation]

        case .siswa:
            self.viewControllers = [navigation1,tabThree]

        }
        
    }

}
