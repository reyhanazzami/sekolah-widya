//
//  ListSiswaTableViewController.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import UIKit

class ListSiswaTableViewController: UITableViewController {
    var model = [SiswaViewModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchSiswa()
    }
    
    func fetchSiswa(){
        let apiService = APIService.shared()

        apiService.request(endPoint: "user",method: .GET, parameters: nil, expecting: [User].self) { result in
            switch result{
            case .success(let result):
                self.model = result.map({SiswaViewModel(siswa: $0)})
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return model.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        let siswa = model[indexPath.row]
        cell.textLabel?.text = siswa.nama
        cell.detailTextLabel?.text = siswa.email

        return cell
    }

}
