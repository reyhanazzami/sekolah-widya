//
//  ViewController.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 06/08/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var buttonLogin: UIButton!
    lazy var validation = ValidationServices()
    lazy var roleUser: roleUser? = .siswa
    var user: User?



    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func buttonLoginPressed(_ sender: UIButton) {
        login(email: textFieldEmail.text, password: textFieldPassword.text)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  let home = segue.destination as? TabBarViewController{
            home.roleUser = self.roleUser!
            home.user = self.user
        }
    }
    
    
    
    func login(email: String?, password: String?){
        do{
            //let email = try validation.valdidateEmail(textFieldEmail.text)
            //let password = try validation.validatePassword(textFieldPassword.text)
            let check = validation.validateEmailRegistered(email)
            if !check{
                showAlert(message: "Email is not registered")
            }else{
                let apiService = APIService.shared()
                apiService.loginUser(endPoint: "user/login", method: .POST, email: email, password: password, expecting: User.self) { result in
                    switch result{
                    case .success(let result):
                        self.user = result
                        if "admin".caseInsensitiveCompare(email!) == ComparisonResult.orderedSame{
                            self.roleUser = .admin
                        }else{
                            self.roleUser = .siswa
                        }
                        DispatchQueue.main.async {
                            self.action()
                        }
                    case .failure(_):
                        DispatchQueue.main.async {
                            self.showAlert(message: "Wrong Password")
                        }
                    }
                }
            }
        }catch{
            showAlert(message: error.localizedDescription)
        }
    }
    
    func action(){
        performSegue(withIdentifier: "toHome", sender: self)

    }
}

