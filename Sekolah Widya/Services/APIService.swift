//
//  APIService.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 06/08/21.
//

import Foundation

class APIService{
    final let constURL = "https://gentle-woodland-43735.herokuapp.com/"

    private static var sharedApiManager: APIService = {
            let apiManager = APIService()
            
            return apiManager
        }()
    
    class func shared() -> APIService {
            return sharedApiManager
        }
        
        // MARK: - Initialization
        
       

    func request<T: Codable>(endPoint: String, method: HTTPMethod,parameters: [URLQueryItem]?, expecting: T.Type, completion: @escaping(Result<T, Error>)-> Void){

        guard let url = URL(string: constURL+endPoint) else{
            completion(.failure(NetworkError.badURL))
            return
        }
        
            var component = URLComponents(url: url, resolvingAgainstBaseURL: false)
            component?.queryItems = parameters
            
            var request = URLRequest(url: (component?.url)!)
            request.httpMethod = method.rawValue
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    
            let task = URLSession.shared.dataTask(with: request){ data, _, error in
                guard let data = data else{
                    if let error = error {
                        completion(.failure(error))
        
                    }else{
                        completion(.failure(NetworkError.badData))
                    }
                    return
                }
                
                do{
                    print(data)
                    let result = try JSONDecoder().decode(expecting, from: data)
                    completion(.success(result))
                }catch{
                    completion(.failure(error))
                }
                
            }
            
            task.resume()
        

    }
    func send<T: Codable>(endPoint: String, method: HTTPMethod,parameters: T, completion: @escaping(Result<T, Error>)-> Void){

        guard let url = URL(string: constURL+endPoint) else{
            completion(.failure(NetworkError.badURL))
            return
        }

        do{
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = try JSONEncoder().encode(parameters)
            
            let dataTask = URLSession.shared.dataTask(with: request){data, response, error in
                guard let httpresponse = response as? HTTPURLResponse, httpresponse.statusCode == 201,
                      let jsonData = data else{
                    completion(.failure(NetworkError.badURL))
                    return
                }
                do{
                    let result = try JSONDecoder().decode(T.self, from: jsonData)
                    print(result)
                    completion(.success(result))
                }catch{
                    completion(.failure(NetworkError.badData))
                }
            }
            dataTask.resume()
        }
        catch{
            completion(.failure(NetworkError.badJSON))
        }
        

    }
    
    func loginUser<T: Codable>(endPoint: String, method: HTTPMethod,email: String?,password: String?, expecting: T.Type, completion: @escaping(Result<T, Error>)-> Void){

        guard let url = URL(string: constURL+endPoint) else{
            completion(.failure(NetworkError.badURL))
            return
        }

        do{
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let data = ["email": email,
                        "password": password]
            request.httpBody = try JSONEncoder().encode(data)
            
            let dataTask = URLSession.shared.dataTask(with: request){data, response, error in
                guard let httpresponse = response as? HTTPURLResponse, httpresponse.statusCode == 201,
                      let jsonData = data else{
                    completion(.failure(NetworkError.badURL))
                    return
                }
                do{
                    let result = try JSONDecoder().decode(expecting, from: jsonData)
                    completion(.success(result))
                }catch{
                    completion(.failure(NetworkError.badData))
                }
            }
            dataTask.resume()
        }
        catch{
            completion(.failure(NetworkError.badJSON))
        }
        

    }
    
}


enum NetworkError: Error {
    case badURL
    case badData
    case badJSON
}

enum HTTPMethod: String{
    case GET
    case POST
}

struct JSONResponse: Codable {
    let success: Bool
    let message, data: String
}
