//
//  ValidationServices.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 06/08/21.
//

import Foundation

struct ValidationServices {
    let apiService = APIService.shared()
    
    func validateEmail(_ email: String?) throws->String {
        guard let email = email else{throw ValidationError.invalidValue}
        guard  email != "" else{throw ValidationError.invalidValue}

            let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
            do {
                let regex = try NSRegularExpression(pattern: emailRegEx)
                let nsString = email as NSString
                let results = regex.matches(in: email, range: NSRange(location: 0, length: nsString.length))
                
                if results.count == 0
                {
                    throw ValidationError.emailFormatWrong
                }
            } catch {
                throw ValidationError.emailFormatWrong
            }
        return email
    }
    
    func validatePassword(_ password: String?) throws-> String{
        guard let password = password else{ throw ValidationError.invalidValue }
        guard password.count >= 8 else {throw ValidationError.passwordTooShort}
        return password
    }
    func validatePasswordConfirmation(_ password: String? , _ passwordConfirmation: String?) throws-> String{
        guard let password = password else{ throw ValidationError.invalidValue }
        guard let passwordConfirmation = passwordConfirmation else{ throw ValidationError.invalidValue }
        guard password == passwordConfirmation else {throw ValidationError.passwordConfirmationNotMatch}
        return password
    }
    func validatePhone(_ phone: String?) throws-> String{
        guard let phone = phone else{ throw ValidationError.invalidValue }
        guard phone.count >= 8 else {throw ValidationError.phoneNumberTooShort}
        guard phone.count <= 20 else {throw ValidationError.phoneNumberTooLong}
        return phone
    }
    
    func validateEmailRegistered(_ email: String?) -> Bool{
        let param = [
            URLQueryItem(name: "email", value: email)
        ]
        var returnValue =  false
        let semaphore = DispatchSemaphore(value: 0)
        apiService.request(endPoint: "user/checkExisting", method: .GET, parameters: param, expecting: User.self) { result in
            switch result{
            case .success(let user):
                if user.email != nil{
                    returnValue = true
                }
                
            case .failure(let error):
                print(error)
            }
            semaphore.signal()

        }
        _ = semaphore.wait(wallTimeout: .distantFuture)

        return returnValue        
    }
    
}

enum ValidationError: LocalizedError{
    case invalidValue
    case passwordConfirmationNotMatch
    case passwordTooShort
    case emailFormatWrong
    case phoneNumberTooShort
    case phoneNumberTooLong
    
    var errorDescription: String?{
        switch self {
        case .invalidValue:
            return "You Have Entered Invalid Value"
        case .passwordConfirmationNotMatch:
            return "Password Confirmation doesn't match"
        case .passwordTooShort:
            return "Your Password is too Short \n Please insert min 8 characters"
        case .emailFormatWrong:
            return "Please insert correct email format"
        case .phoneNumberTooShort:
            return "Phone Number is too Short \n Please insert min 8 number"
        case .phoneNumberTooLong:
            return "Your Password is too Long \n Please insert max 20 number"
        }
    }
}

enum LoginValidation: LocalizedError{
    case invalidPassword
    case invalidEmail
    
    var errorDescription: String?{
        switch self {
        case .invalidPassword:
            return "Wrong Password"
        case .invalidEmail:
            return "Email is not registered"
        }
    }
}
