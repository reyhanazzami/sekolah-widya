//
//  Extension.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import Foundation
import  UIKit

extension UIViewController{
    func showAlert(message: String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)

    }
}

enum roleUser{
    case admin
    case siswa
}
