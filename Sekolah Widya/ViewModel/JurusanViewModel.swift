//
//  JurusanViewModel.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import Foundation

struct JurusanViewModel {
    var namaJurusan: String?
    var kuotaMaks: Int?
    
    init(jurusan: Jurusan) {
        self.namaJurusan = jurusan.namaPenjurusan
        self.kuotaMaks = jurusan.kuotaMaks
    }
}
