//
//  SiswaViewModel.swift
//  Sekolah Widya
//
//  Created by Reyhan Rifqi on 07/08/21.
//

import Foundation
import UIKit

struct SiswaViewModel {
    
    var nama: String?
    var email: String?
    var alamat: String?
    var telepon: String?
    var sekolah: String?
    var namaOrtu: String?
    var photoURL: String?
    
    init(siswa: User) {
        self.nama = siswa.nama
        self.email = siswa.email
        self.alamat = siswa.alamat
        self.telepon = siswa.noTelp
        self.sekolah = siswa.sekolahAsal
        self.namaOrtu = siswa.namaOrtu
        self.photoURL = siswa.foto
        
    }
    
    func loadImage(completion: @escaping(UIImage?)-> Void){
        guard let url = photoURL else {return}
        guard let url = URL(string: url) else { return }
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            guard let data = data else { return }
            let image = UIImage(data: data)
            completion(image)
        }.resume()
    }
}
